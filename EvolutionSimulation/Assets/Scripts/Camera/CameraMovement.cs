﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
	float speed = 5.0f;

	public int UpdatePeriod;
	private int cUpdateCount;

	public Text CellInspectorText;

	// For following selected
	private bool selected;
	private Cell selectedCell;
	private Transform selectedTransform;

	// For Debug Info
	private bool showBiases = false;
	private bool showWeights = false;
	private bool showActivations = false;

	public GameObject ScriptInjector;

	private float timeScale = 1.0f;

	private void Start()
	{
		this.resetSelection();
	}

	private void Update()
	{
		// Clear following if it is no longer existing
		if (this.selectedCell == null && this.selected)
			this.resetSelection();

		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
			this.transform.Translate(new Vector3 (this.speed * Time.deltaTime,0,0));
		else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
			this.transform.Translate(new Vector3(0, (-1) * this.speed * Time.deltaTime, 0));
		else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
			this.transform.Translate(new Vector3( (-1) * this.speed * Time.deltaTime, 0, 0));
		else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
			this.transform.Translate(new Vector3(0, this.speed * Time.deltaTime, 0));
		else if (Input.GetKey(KeyCode.Period))
			this.transform.Translate(new Vector3(0, 0, this.speed * Time.deltaTime));
		else if (Input.GetKey(KeyCode.Comma))
			this.transform.Translate(new Vector3(0, 0, (-1) * this.speed * Time.deltaTime));

		// Show visibility cone of currently selected
		if (Input.GetKeyDown(KeyCode.C))
			this.selectedCell.toggleVisibilityCones();

		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (this.timeScale == 1.0f)
			{
				this.timeScale = 0.0f;
				Time.timeScale = this.timeScale;
			}
			else
			{
				this.timeScale = 1.0f;
				Time.timeScale = this.timeScale;
			}
		}

		if (Input.GetKeyDown(KeyCode.V))
		{
			this.toggleActivationsDisplay();
			if (this.selectedCell != null && this.selected)
				this.printBody(this.selectedCell.GetBody());
		}

		if (Input.GetKeyDown(KeyCode.B))
		{
			this.toggleBiasDisplay();
			if (this.selectedCell != null && this.selected)
				this.printBody(this.selectedCell.GetBody());
		}

		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.G))
		{
			this.toggleWeightsDisplay();
			if (this.selectedCell != null && this.selected)
				this.printBody(this.selectedCell.GetBody());
		}
		
		this.checkForKeyPress();
		
		if (this.selectedCell != null && this.selected)
				this.updateCameraPosition();

		if (this.cUpdateCount > this.UpdatePeriod)
		{
			if (this.selectedCell != null && this.selected)
				this.printBody(this.selectedCell.GetBody());

			this.cUpdateCount = 0;
		}

		// this.printAllBodys();

		this.cUpdateCount++;
	}

	private void toggleActivationsDisplay()
	{
		this.showActivations = !this.showActivations;
		Debug.Log("Toggeling Activations Display");
	}

	private void toggleBiasDisplay()
	{
		this.showBiases = !this.showBiases;
		Debug.Log("Toggeling Bias Display");
	}

	private void toggleWeightsDisplay()
	{
		this.showWeights = !this.showWeights;
		Debug.Log("Toggeling Weights Display");
	}

	private void updateCameraPosition()
	{
		this.transform.position = new Vector3(this.selectedTransform.position.x, this.transform.position.y, this.selectedTransform.position.z);
	}

	private void checkForKeyPress()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 100))
			{
				string name = hit.transform.gameObject.name;

				if (name.Equals("Cell(Clone)"))
				{
					Cell cCell = hit.transform.gameObject.GetComponent<Cell>();
					this.printBody(cCell.GetBody());

					this.selectFollowing(hit.transform, cCell);
				}
				else if (name.Equals("Food(Clone)"))
				{
					Food cFood = hit.transform.gameObject.GetComponent<Food>();
					this.CellInspectorText.text = "Following Food with Id: " + cFood.FoodId.ToString();

					this.selectFollowing(hit.transform, null);
				}
				else
				{
					this.resetSelection();
					this.unselect();
				}
			}
		}
	}

	private void selectFollowing(Transform transform, Cell _cell)
	{
		this.selected = true;
		this.selectedTransform = transform;
		this.selectedCell = _cell;
	}

	private void unselect()
	{
		this.selected = false;
		this.selectedTransform = null;
		this.selectedCell = null;
	}

	private void resetSelection()
	{
		this.CellInspectorText.text = "Nothing Selected";
		this.CellInspectorText.text += this.printScores();

		this.selected = false;
		this.selectedTransform = null;
		this.selectedCell = null;
	}

	private void printBody(Body body)
	{
		string toDisplay = "Following a Cell\tFood: " + body.GetParent().FoodReserves + "\t" + body.GetParent().GetScore() + "\n";
		toDisplay += "Sensors[" + body.Inputs.Count + "], ";
		toDisplay += "Neurons[" + body.Neurons.Count + "], ";
		toDisplay += "Actors[" + body.Actors.Count + "]\n";

		if (this.showActivations)
			toDisplay = this.printActivations(toDisplay, body);

		if (this.showWeights)
			toDisplay = this.printWeights(toDisplay, body);

		if (this.showBiases)
			toDisplay = this.printBias(toDisplay, body);

		this.CellInspectorText.text = toDisplay;
	}

	private string printActivations(string toDisplay, Body body)
	{
		toDisplay += "Activations\n";

		toDisplay += "\tS:[ ";
		for (int i = 0; i < body.Inputs.Count; i++)
		{
			toDisplay += body.Inputs[i].GetActivation().ToString();

			if (i < body.Inputs.Count - 1)
				toDisplay += "|";
		}
		toDisplay += "]\n";

		toDisplay += "\tN:[ ";
		for (int i = 0; i < body.Neurons.Count; i++)
		{
			toDisplay += body.Neurons[i].GetActivation().ToString();

			if (i < body.Neurons.Count - 1)
				toDisplay += "|";
		}
		toDisplay += "]\n";

		toDisplay += "\tA:[ ";
		for (int i = 0; i < body.Actors.Count; i++)
		{
			toDisplay += body.Actors[i].GetActivation().ToString();

			if (i < body.Actors.Count - 1)
				toDisplay += "|";
		}
		toDisplay += "]\n";

		return toDisplay;
	}

	private string printWeights(string toDisplay, Body body)
	{
		toDisplay += "Weights\n";
		toDisplay += "\tS:[\tNO WEIGHTS]\n";

		toDisplay += "\tN:[ ";
		for (int i = 0; i < body.Neurons.Count; i++)
		{
			if (i == 0)
				toDisplay += "\tN_" + i.ToString() + ":";
			else
				toDisplay += "\t\t\tN_" + i.ToString() + ":";

			for (int j = 0; j < body.Neurons[i].weights.Count; j ++)
			{
				toDisplay += body.Neurons[i].weights[j];
				if (j < body.Neurons[i].weights.Count - 1)
					toDisplay += "|";
			}

			if (i < body.Neurons.Count - 1)
				toDisplay += "\n";
		}
		toDisplay += "]\n";

		toDisplay += "\tA:[ ";
		for (int i = 0; i < body.Actors.Count; i++)
		{
			if (i == 0)
				toDisplay += "\tA_" + i.ToString() + ":";
			else
				toDisplay += "\t\t\tA_" + i.ToString() + ":";

			for (int j = 0; j < body.Actors[i].weights.Count; j++)
			{
				toDisplay += body.Actors[i].weights[j];
				if (j < body.Actors[i].weights.Count - 1)
					toDisplay += "|";
			}

			if (i < body.Actors.Count - 1)
				toDisplay += "\n";
		}
		toDisplay += "]\n";

		return toDisplay;
	}

	private string printBias(string toDisplay, Body body)
	{
		toDisplay += "Biases\n";
		toDisplay += "\tS:[\t";

		for (int i = 0; i < body.Inputs.Count; i++)
		{
			toDisplay += body.Inputs[i].Bias.ToString();

			if (i < body.Inputs.Count - 1)
				toDisplay += "|";
		}
		toDisplay += "]\n";

		toDisplay += "\tN:[\t";

		for (int i = 0; i < body.Neurons.Count; i++)
		{
			toDisplay += body.Neurons[i].Bias.ToString();

			if (i < body.Neurons.Count - 1)
				toDisplay += "|";
		}
		toDisplay += "]\n";

		toDisplay += "\tA:[\t";

		for (int i = 0; i < body.Actors.Count; i++)
		{
			toDisplay += body.Actors[i].Bias.ToString();

			if (i < body.Actors.Count - 1)
				toDisplay += "|";
		}
		toDisplay += "]\n";

		return toDisplay;
	}

	private string printScores()
	{
		GeneralFactory factory = this.ScriptInjector.GetComponent<GeneralFactory>();

		try
		{
			return "\nScores Sum: " + factory.cellFactory.CellsHighscore.GetScores() + "\n";
		}
		catch (Exception e)
		{
			Debug.Log("Not yet initialized: " + e.Message);
			return "\nScores not yet inited\n";
		}
		
	}

	private void printAllBodys()
	{
		this.CellInspectorText.text = "";
		GeneralFactory factory = this.ScriptInjector.GetComponent<GeneralFactory>();

		foreach(Cell cCell in factory.cellFactory.GetCells())
		{
			string toDisplay = "Following a Cell\tFood: " + cCell.GetBody().GetParent().FoodReserves + "\t" + cCell.GetBody().GetParent().GetScore() + "\n";
			toDisplay += "Sensors[" + cCell.GetBody().Inputs.Count + "], ";
			toDisplay += "Neurons[" + cCell.GetBody().Neurons.Count + "], ";
			toDisplay += "Actors[" + cCell.GetBody().Actors.Count + "]";

			this.CellInspectorText.text += "\n" + toDisplay;
		}
	}
}
