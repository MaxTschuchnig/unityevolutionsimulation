﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class VisualSensor : Sensor
{
	[NonSerialized]
	public VisibilityCone ChildVisibilityCone;

	#region private properties
	[NonSerialized]
	Cell parent;

	float Angle			{ get; set; }
	float Range			{ get; set; } // [0-179]
	float Direction		{ get; set; } // [0-359]
	bool TypeClosest	{ get; set; }
	#endregion

	#region public methods
	public VisualSensor(Cell _parent, RandomGenerator rand, float _visualRangeMu, float _visualRangeStd, bool _typeClosest, float _bias)
	{
		this.parent = _parent;

		this.Angle = rand.GetRandomFloat(180);
		this.Direction = rand.GetRandomFloat(360);
		this.Range = rand.GetNormalDistNumber(_visualRangeMu, _visualRangeStd);
		this.TypeClosest = _typeClosest;

		this.Bias = _bias;
	}

	// Looking takes 0.1 base + 0.1 * Angle if smaller 85 else 2^Range + 1.1^(Range)
	public override double GetEnergyConsumption()
	{
		double consumption = 0.1 + Math.Pow(1.025, this.Range);

		if (this.Angle <= 85)
			consumption += (0.1 * this.Angle);
		else
			consumption += Math.Pow(1.2, this.Angle - 85 + (0.1 * this.Angle));
		
		return consumption * 0.05;
	}

	public void AddToAngle(float _angle)
	{
		this.Angle += _angle;
	}

	public void AddToRange(float _range)
	{
		this.Range += _range;
	}

	public override double GetActivation()
	{
		List<Transform> foodList =  this.ChildVisibilityCone.GetActuallySeenFood(this.parent);
		double activation = 1;

		// Return the exact distance of the closest
		if (TypeClosest)
		{
			// Get closest distance
			float cDistance = 0;
			float shortestDistance = 100000;
			foreach (Transform cFood in foodList)
			{
				cDistance = Vector3.Distance(this.parent.transform.position, cFood.position);
				if (cDistance < shortestDistance)
					shortestDistance = cDistance;
			}

			if (shortestDistance == 100000)
				shortestDistance = -1;

			activation = shortestDistance;

			
				
		}
		// Else return the sum of all Distances
		else
			foreach (Transform cFood in foodList)
				activation += Vector3.Distance(this.parent.transform.position, cFood.position);

		return activation;
	}

	public float GetRange()
	{
		return this.Range;
	}

	public float GetDirection()
	{
		return this.Direction;
	}

	public float GetAngle()
	{
		return this.Angle;
	}
	#endregion

	public void SetParent(Cell _parent)
	{
		this.parent = _parent;
	}
}