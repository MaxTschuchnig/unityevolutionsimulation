﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class MemoryNeuron : Sensor
{
	#region public properties
	public List<float> weights { get; set; }
	#endregion

	#region public methods
	public MemoryNeuron(float _bias)
	{
		this.Bias = _bias;
	}

	// Membering takes 1 Energy
	public override double GetEnergyConsumption()
	{
		return 1;
	}

	public override double GetActivation()
	{
		// TODO: Implement based on raycasting!!!
		return -1.0;
	}
	#endregion
}
