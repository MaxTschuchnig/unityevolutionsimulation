﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Body
{
	#region public properties
	public List<Sensor> Inputs { get; set; }
	public List<Neuron> Neurons { get; set; }
	public List<Actor> Actors { get; set; }

	// For Score
	public int survivedUpdates;
	public int eatenFood;

	// For Families in Highscore List
	public int Family;
	public int SubFamily;
	public int MutationCount = 0;

	#endregion

	public float OnlyDebugScore;

	[NonSerialized]
	private Cell parent;

	#region public methods
	public Body(Cell _parent)
	{
		this.Inputs = new List<Sensor>();
		this.Neurons = new List<Neuron>();
		this.Actors = new List<Actor>();

		this.parent = _parent;
	}

	public Cell GetParent()
	{
		return this.parent;
	}

	public void SetParent(Cell _parent)
	{
		this.parent = _parent;
	}

	internal float GetScore()
	{
		float _score = this.survivedUpdates;
		if (this.parent != null) // Takes a while to init
		{
			_score =+ this.eatenFood * this.parent.GetFactory().FoodEatenModifier;
			float cModifier = 0, sModifier = 0, aModifier = 0;

			// Add complexity modifier if there is inputs, outputs and at least 4 Neurons
			if (this.Inputs.Count > 1 && this.Neurons.Count > 3 && this.Actors.Count > 1)
				cModifier = _score * this.parent.GetFactory().ComplexityModifier;

			// If there is more than three inputs or actors add modifiers
			if (this.Inputs.Count > 3)
				sModifier = _score * this.parent.GetFactory().VisualModifier;

			if (this.Actors.Count > 3)
				aModifier = _score * this.parent.GetFactory().MovementModifier;

			_score += cModifier + sModifier + aModifier;

			if (this.Inputs.Count == 0)
				_score /= 10;

			if (this.Neurons.Count == 0)
				_score /= 50;

			if (this.Actors.Count == 0)
				_score /= 10;

			if (this.Actors.Count == 0 && this.Neurons.Count == 0 && this.Inputs.Count == 0)
			{
				Debug.Log("0-Score");
				_score = 0;
			}
		}

		OnlyDebugScore = _score;

		return _score;
	}
	#endregion
}
