﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MovementActor : Actor
{
	#region private properties
	float Direction { get; set; }
	float Speed { get; set; }
	#endregion

	public float cSpeed { get; set; }

	#region public methods
	public MovementActor(RandomGenerator rand, float _movementSpeedMu, float _movementSpeedstd, float _bias)
	{
		this.Direction = rand.GetRandomFloat(360);
		this.Speed = rand.GetNormalDistNumber(_movementSpeedMu, _movementSpeedstd);

		this.Bias = _bias;

		this.cSpeed = 0;
	}

	public override void Act()
	{
		// if Activation is between -0.5 and 0.5, nothing happens. 
		// if Activation is smaller, it will break by (int) value -0.5
		// if Activation is higher, it will accelerate by (int) value -0.5
		
		double activation = this.GetActivation();
		double value = activation;
		if (activation < 1.35)
		{
			value -= 0.5;
			
			// update speed
			this.cSpeed -= (float) value * 0.1f;
			if (this.cSpeed < 0)
				this.cSpeed = 0; // Cannot go slower than 0
		}
		else if (activation > 1.65)
		{
			value += 0.5;

			// update speed
			this.cSpeed += (float)value * 0.1f;
			if (this.cSpeed > this.Speed)
				this.cSpeed = this.Speed; // Cannot go faster than max speed
		}
		// else, do nothing
	}

	// Consumes 0.5 base + 1.75^Speed
	public override double GetEnergyConsumption()
	{
		double consumption = 0.5 + Math.Pow(1.75, this.Speed);

		return consumption; // make movement extremly cheap
	}

	public override double GetActivation()
	{
		double activation = 0;

		for (int i = 0; i < this.weights.Count; i++)
		{
			// Implizit casting to double
			activation += this.weights[i] * this.X[i];
		}
		activation += Bias;

		// Return the applied logistic transfer function
		return 1 / 1 + System.Math.Exp((-1) * activation);
	}

	public void AddToAngle(float angle)
	{
		this.Direction += angle;
	}

	public void AddToSpeed(float _speed)
	{
		this.Speed += _speed;
	}

	internal Vector3 GetDirection()
	{
		Vector3 direction = new Vector3(Mathf.Cos(this.ConvertToRadians(this.Direction)), 0, Mathf.Sin(this.ConvertToRadians(this.Direction)));
		direction = direction * this.cSpeed;

		return direction;
	}

	private float ConvertToRadians(float angle)
	{
		return (Mathf.PI / 180) * angle;
	}
	#endregion
}
