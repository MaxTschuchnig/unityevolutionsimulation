﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EatingActor : Actor
{
	public bool Eating;

	#region public methods
	public EatingActor(float _bias)
	{
		this.Bias = _bias;
		this.Eating = false;
	}

	public override void Act()
	{
		if (this.GetActivation() > 1.5)
			this.Eating = true;
		else
			this.Eating = false;
	}

	// Eating action takes 2 Energy 
	public override double GetEnergyConsumption()
	{
		return 1.5;
	}

	public override double GetActivation()
	{
		double activation = 0;

		try
		{
			for (int i = 0; i < this.weights.Count; i++)
			{
				// Implizit casting to double
				activation += this.weights[i] * this.X[i];
			}
			activation += Bias;
		}
		catch (Exception e)
		{
			activation = 0;
			Debug.Log("What is happening in here?!?!?!?: " + e.Message);
		}

		// Return the applied logistic transfer function
		return 1 / 1 + System.Math.Exp((-1) * activation);
	}
	#endregion
}
