﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public abstract class Actor : IBodyPart
{
	#region public properties
	public List<float> weights { get; set; }
	#endregion

	#region public methods
	public abstract void Act();
	#endregion
}