﻿using System;
using System.Collections;
using System.Collections.Generic;

// Only single layer for now, should suffice
[Serializable]
public class Neuron : IBodyPart
{
	#region public properties
	public List<float> weights { get; set; }
	#endregion

	#region public methods
	public Neuron(float _bias)
	{
		this.Bias = _bias;
	}

	// A neuron takes 0.1 per weight
	public override double GetEnergyConsumption()
	{
		return (0.02) * this.weights.Count;
	}

	public override double GetActivation()
	{
		double activation = 0;

		for (int i = 0; i < this.weights.Count; i++)
		{
			// Implizit casting to double
			activation += this.weights[i] * this.X[i];
		}
		activation += Bias;

		// Return the applied logistic transfer function
		return 1 / 1 + System.Math.Exp((-1) * activation);
	}
	#endregion
}
