﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
	public int UpdatePeriod;
	private int cUpdateCount;

	public bool Eating;
	public float FoodReserves = 10000;
	private float foodFromMovement;

	// In order to only store cells that actually move
	public float SpeedSum = 0;

	Body _body;
	Rigidbody _rigidbody;
	float _direction;

	private GeneralFactory generalFactory;

	private bool VisibilityCones = false;

	public int MutationCounter;
	bool alreadyHasBody = false;

	void Start()
    {
		// Fetch the Rigidbody component you attach from your GameObject
		this._rigidbody = this.GetComponent<Rigidbody>();

		// by using int, this is exclusive the upper border
		this._direction = UnityEngine.Random.Range(0, 360);

		// Get food Factory to correctly destroy food
		GameObject _foodFactory = GameObject.Find("ScriptInjector");
		this.generalFactory = _foodFactory.gameObject.GetComponent<GeneralFactory>();

		this.GenerateBody();
	}

	public void GenerateBody()
	{
		// Get a body if it hasn't already have one
		if (!this.alreadyHasBody)
			this._body = generalFactory.cellBodyFactory.GetBody(this);

		this._body.survivedUpdates = 0;
		this.foodFromMovement = 0;

		this.generateEyes();
	}

	internal void SetBody(Body body)
	{
		this._body = body;
		this.alreadyHasBody = true;
	}

	internal void SetEyesCellParent()
	{
		foreach (Sensor cSensor in this._body.Inputs)
		{
			if (cSensor is VisualSensor)
			{
				VisualSensor cVisualSensor = cSensor as VisualSensor;
				cVisualSensor.SetParent(this);
			}
		}
	}

	public void generateEyes()
	{
		// Check if this body inputs has visual sensors
		int i = 0;
		foreach (Sensor cInput in this._body.Inputs)
		{
			if (cInput is VisualSensor)
			{
				// Add visibility Cone
				if (this.generalFactory == null)
				{
					GameObject _foodFactory = GameObject.Find("ScriptInjector");
					this.generalFactory = _foodFactory.gameObject.GetComponent<GeneralFactory>();
				}

				GameObject _cVisibleCone = this.generalFactory.InstantiateGameObject(this.generalFactory.VisibleCone,
					new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
				_cVisibleCone.transform.parent = this.transform;
				_cVisibleCone.transform.Rotate(0, 180, 0);

				// Edit to actualy represent the correct sensors
				VisualSensor cVisualSensor = cInput as VisualSensor;
				// Edit for correct range
				_cVisibleCone.transform.localScale = new Vector3(1, 1, cVisualSensor.GetRange());

				// Edit to get correct rotation
				_cVisibleCone.transform.Rotate(new Vector3(0, cVisualSensor.GetDirection(), 0));

				// Edit to start seeing in body
				Vector3 currentDir = _cVisibleCone.transform.rotation * Vector3.forward;
				currentDir = currentDir.normalized * (cVisualSensor.GetRange() / 4);
				_cVisibleCone.transform.position -= new Vector3(currentDir.x, 0.0f, currentDir.z);


				// Edit to update range of seeing (sine rule)
				float alpha = cVisualSensor.GetAngle();
				float beta = 180 - 90 - (alpha / 2);
				float opposite = cVisualSensor.GetRange() / Mathf.Sin(beta) * Mathf.Sin(alpha / 2);
				if (Mathf.Abs(opposite) > Mathf.Abs(cVisualSensor.GetRange()))
					opposite = cVisualSensor.GetRange();
				_cVisibleCone.transform.localScale = new Vector3(opposite, _cVisibleCone.transform.localScale.y, _cVisibleCone.transform.localScale.z);


				// Set parent sensor to fill current activation with cone view
				VisibilityCone temp = _cVisibleCone.GetComponent<VisibilityCone>();
				temp.parentSensor = cVisualSensor;
				temp.ConeId = i;
				cVisualSensor.ChildVisibilityCone = temp;

				i++;
			}
		}
	}

	internal void toggleVisibilityCones()
	{
		foreach (Sensor cInput in this._body.Inputs)
		{
			if (cInput is VisualSensor)
			{
				VisualSensor cVisualSensor = cInput as VisualSensor;

				cVisualSensor.ChildVisibilityCone.gameObject.GetComponent<Renderer>().enabled = this.VisibilityCones;
			}
		}

		this.VisibilityCones = !this.VisibilityCones;
	}

	public void UpdateMe()
	{
		// Acting based on activations
		Vector3 MovementVector = new Vector3();
		Vector3 cMovementVector = new Vector3();
		if (this._body != null && this._body.Inputs != null && this._body.Neurons != null && this._body.Actors != null)
		{
			// Subtract Food, after Body has been generated
			this.UseFood();

			// Add reaction time
			if (this.cUpdateCount > this.UpdatePeriod)
			{
				// Get Input for Neurons
				if (this._body.Neurons.Count > 0 && this._body.Inputs.Count > 0)
					for (int neuronCount = 0; neuronCount < this._body.Neurons.Count; neuronCount++)
						for (int sensorCount = 0; sensorCount < this._body.Inputs.Count; sensorCount++)
							this._body.Neurons[neuronCount].X[sensorCount] = (float)this._body.Inputs[sensorCount].GetActivation();

				// Get Input for Actors
				if (this._body.Actors.Count > 0 && this._body.Neurons.Count > 0)
					for (int actorCount = 0; actorCount < this._body.Actors.Count; actorCount++)
						for (int neuronCount = 0; neuronCount < this._body.Neurons.Count; neuronCount++)
							this._body.Actors[actorCount].X[neuronCount] = (float)this._body.Neurons[neuronCount].GetActivation();

				this.cUpdateCount = 0;
			}
			this.cUpdateCount++;

			foreach (Actor cActor in this._body.Actors)
			{
				// Get current Acting values
				cActor.Act();
				
				if (cActor is MovementActor)
				{
					MovementActor cMovementActor = cActor as MovementActor;
					cMovementVector = cMovementActor.GetDirection();
					MovementVector += cMovementVector;
				}
			}

			this.MutationCounter++;
		}

		if (this._rigidbody != null)
		{
			Vector3 target = transform.forward;
			target.x = MovementVector.x;
			target.z = MovementVector.z;
			this._rigidbody.velocity = MovementVector * Vector3.Magnitude(MovementVector) * 0.1f;
			this._direction = Vector3.Angle(Vector3.forward, MovementVector);

			// Show Direction
			Debug.DrawRay(this.transform.position, MovementVector, Color.red);

			this.foodFromMovement = Vector3.Magnitude(MovementVector) * 2.5f;
			this.SpeedSum += Vector3.Magnitude(MovementVector);

			Quaternion _rotation = Quaternion.Euler(0, this._direction, 0);
			this.transform.rotation = _rotation;
		}

		if (this._body != null)
			this._body.survivedUpdates++;
	}

	internal float GetScore()
	{
		return this._body.GetScore();
	}

	public float GetDirection()
	{
		return this._direction;
	}

	public void SetDirection(float newDirection)
	{
		this._direction = newDirection;
	}

	public GeneralFactory GetFactory()
	{
		return this.generalFactory;
	}

	public Body GetBody()
	{
		if (this.generalFactory == null)
		{
			// Get food Factory to correctly destroy food
			GameObject _foodFactory = GameObject.Find("ScriptInjector");
			this.generalFactory = _foodFactory.gameObject.GetComponent<GeneralFactory>();
		}

		if (this._body == null)
		{
			this._body = generalFactory.cellBodyFactory.GetBody(this);
			this.alreadyHasBody = true;
		}

		return this._body;
	}

	private void UseFood()
	{
		// Min value is 1.5
		float usedFood = 1.5f;

		foreach (Sensor cSensor in this._body.Inputs)
			usedFood += (float) cSensor.GetEnergyConsumption();

		foreach (Neuron cNeuron in this._body.Neurons)
			usedFood += (float)cNeuron.GetEnergyConsumption();

		foreach (Actor cActor in this._body.Actors)
			usedFood += (float)cActor.GetEnergyConsumption();

		usedFood += this.foodFromMovement;

		this.FoodReserves -= usedFood;
		if (this.FoodReserves < 0)
			this.DestroyMe();
	}

	private void DestroyMe()
	{
		this.generalFactory.cellFactory.AddToDestroy(this);
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (this.generalFactory != null)
		{
			Food collidedFood = collision.gameObject.GetComponent<Food>();
			if (collidedFood != null)
			{
				this.EatFood(collidedFood);
			}
		}
	}

	private void EatFood(Food collidedFood)
	{
		// Look if cell has a mouth currently eating
		try
		{
			foreach (Actor cActor in this._body.Actors)
			{
				if (cActor is EatingActor)
				{
					EatingActor cMouth = cActor as EatingActor;
					if (cMouth.Eating)
					{
						this.generalFactory.foodFactory.DestroyFood(collidedFood);
						this.FoodReserves += this.generalFactory.FoodNoms;
						this._body.eatenFood++;
					}
				}
			}
		}
		catch (Exception e)
		{
			Debug.LogError("Error when deleting food: " + e.Message);
		}
	}
}
