﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibilityCone : MonoBehaviour
{
	public VisualSensor parentSensor;
	public int ConeId;

	public List<Food> FoodTransforms;

	// Just for debug
	public List<int> FoodInRange;

	public List<Transform> GetActuallySeenFood(Cell RaycastSources)
	{
		int layer_mask = LayerMask.GetMask("Food");

		List<Transform> actuallySeenFood = new List<Transform>();

		Vector3 Diff;
		RaycastHit hit;

		foreach (Food cFood in this.FoodTransforms)
		{
			if (cFood == null)
				continue;

			if (RaycastSources == null)
			{
				Debug.Log("Some error occured in GetActuallySeenFood. Are Eyes and Visibility Cone correctly connected to body?");
				continue;
			}
				

			Diff = cFood.transform.position - RaycastSources.transform.position;
		
			if (Physics.Raycast(RaycastSources.transform.position, Diff, out hit, 50, layer_mask))
			{
				Debug.DrawRay(RaycastSources.transform.position, Diff, Color.green);
				actuallySeenFood.Add(cFood.transform);
			}
		}

		return actuallySeenFood;
	}

	private void Start()
	{
		// add isTrigger
		MeshCollider boxCollider = gameObject.AddComponent<MeshCollider>();
		boxCollider.convex = true;
		boxCollider.isTrigger = true;

		// Make invisible to start with
		this.gameObject.GetComponent<Renderer>().enabled = false;
	}

	// Add to list
	private void OnTriggerEnter(Collider collision)
	{
		Food collidedFood = collision.gameObject.GetComponent<Food>();
		if (collidedFood != null)
		{
			this.FoodTransforms.Add(collidedFood);
			this.FoodInRange.Add(collidedFood.FoodId);
		}
	}

	// Remove from List
	private void OnTriggerExit(Collider collision)
	{
		Food collidedFood = collision.gameObject.GetComponent<Food>();
		if (collidedFood != null)
		{
			this.FoodTransforms.Remove(collidedFood);
			this.FoodInRange.Remove(collidedFood.FoodId);
		}
	}
}
