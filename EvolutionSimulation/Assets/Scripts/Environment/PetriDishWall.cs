﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetriDishWall : MonoBehaviour {

	GameObject factory;

	private void Start()
	{
		this.factory = GameObject.Find("ScriptInjector");
	}

	private void OnCollisionEnter(Collision collision)
	{
		float diff = 1f;

		if (this.factory != null)
		{
			Vector2 planetSize = this.factory.GetComponent<GeneralFactory>().PlaneSize;
			this.foodCollision(collision, planetSize, diff);
			this.cellCollision(collision, planetSize, diff);
		}
	}

	private void foodCollision(Collision collision, Vector2 planetSize, float diff)
	{
		Food collidedFood = collision.gameObject.GetComponent<Food>();

		if (collidedFood != null)
		{
			float currentDirection = collidedFood.GetDirection();
			if (currentDirection < 0)
				currentDirection = 360 + currentDirection;

			float directionChange = 0;

			if (collidedFood.transform.position.x > (planetSize.x - diff))
			{
				if (currentDirection >= 0 && currentDirection <= 90)
					directionChange = 270;
				else
					directionChange = 90;
			}
			else if (collidedFood.transform.position.x < (((-1) * planetSize.x) + diff))
			{
				if (currentDirection >= 270 && currentDirection <= 360)
					directionChange = 90;
				else
					directionChange = 270;
			}
			else if (collidedFood.transform.position.z > (planetSize.y - diff))
			{
				if (currentDirection >= 0 && currentDirection <= 90)
					directionChange = 90;
				else
					directionChange = 270;
			}
			else if (collidedFood.transform.position.z < (((-1) * planetSize.y) + diff))
			{
				if (currentDirection >= 90 && currentDirection <= 180)
					directionChange = 270;
				else
					directionChange = 90;
			}

			float direction = (currentDirection + directionChange) % 360;
			collision.gameObject.GetComponent<Food>().SetDirection(direction);
		}
	}

	private void cellCollision(Collision collision, Vector2 planetSize, float diff)
	{
		Cell collidedCell = collision.gameObject.GetComponent<Cell>();

		if (collidedCell != null)
		{
			float currentDirection = collidedCell.GetDirection();
			if (currentDirection < 0)
				currentDirection = 360 + currentDirection;

			float directionChange = 0;

			if (collidedCell.transform.position.x > (planetSize.x - diff))
			{
				if (currentDirection >= 0 && currentDirection <= 90)
					directionChange = 270;
				else
					directionChange = 90;
			}
			else if (collidedCell.transform.position.x < (((-1) * planetSize.x) + diff))
			{
				if (currentDirection >= 270 && currentDirection <= 360)
					directionChange = 90;
				else
					directionChange = 270;
			}
			else if (collidedCell.transform.position.z > (planetSize.y - diff))
			{
				if (currentDirection >= 0 && currentDirection <= 90)
					directionChange = 90;
				else
					directionChange = 270;
			}
			else if (collidedCell.transform.position.z < (((-1) * planetSize.y) + diff))
			{
				if (currentDirection >= 90 && currentDirection <= 180)
					directionChange = 270;
				else
					directionChange = 90;
			}

			float direction = (currentDirection + directionChange) % 360;
			collision.gameObject.GetComponent<Cell>().SetDirection(direction);
		}
	}
}