﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour {

	public float FoodSpeed;
	public int FoodId;

	Rigidbody _rigidbody;
	float _direction;

	void Start()
	{
		// Fetch the Rigidbody component you attach from your GameObject
		this._rigidbody = this.GetComponent<Rigidbody>();

		// by using int, this is exclusive the upper border
		this._direction = Random.Range(0, 360);
	}

	public void UpdateMe()
	{
		Quaternion _rotation = Quaternion.Euler(0, this._direction, 0);
		this.transform.rotation = _rotation;

		if (this._rigidbody != null)
			this._rigidbody.velocity = transform.forward * this.FoodSpeed;
	}

	public float GetDirection()
	{
		return this._direction;
	}

	public void SetDirection(float newDirection)
	{
		this._direction = newDirection;
	}
}
