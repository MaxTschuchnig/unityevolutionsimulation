﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class CellFactory
{
	private GeneralFactory generalFactory;
	private List<Cell> cells = new List<Cell>();
	private List<Cell> cellsToDestroy = new List<Cell>();
	private List<Cell> cellsToAddMutation = new List<Cell>();

	public SortedUnityList CellsHighscore;

	private int FamilyCounter = 0;
	private int SubFamilyCounter = 0;

	public CellFactory(GeneralFactory generalFactory)
	{
		this.generalFactory = generalFactory;
		this.CellsHighscore = new SortedUnityList(this.generalFactory.HighscoreCellAmount);
	}

	internal void UpdateCells()
	{
		if (this.cellsToDestroy.Count > 0)
			foreach (Cell cCell in this.cellsToDestroy)
				this.DestroyCell(cCell);

		if (cellsToDestroy.Count > 0)
			this.cellsToDestroy = new List<Cell>();

		foreach (Cell cCell in this.cells)
		{
			cCell.UpdateMe();

			if (cCell.FoodReserves > this.generalFactory.FoodReservesToMutate && cCell.MutationCounter > this.generalFactory.MutationCounterToMutate)
			{
				// Only mutate if minimal amount of movement was reached
				if (cCell.SpeedSum > this.generalFactory.MinimalMovement)
				{
					Cell mutationResult = this.MutateCell(cCell);
					if (mutationResult != null)
						this.cellsToAddMutation.Add(mutationResult);

					cCell.MutationCounter = 0;
				}
			}
		}

		this.cells.AddRange(this.cellsToAddMutation);

		if (cellsToAddMutation.Count > 0)
			this.cellsToAddMutation = new List<Cell>();
	}

	private Cell MutateCell(Cell parent)
	{
		RandomGenerator rng = new RandomGenerator();
		int choice = 0;
		for (int i = 0; i < 100; i++)
			choice = rng.GetRandomInt(8);

		Vector3 spawnPosition = parent.transform.position;

		spawnPosition.y += 0.01f;
		if (choice == 0) // North
		{
			spawnPosition.x += 0;
			spawnPosition.z += 1;
		}
		else if (choice == 1) // NorthEast
		{
			spawnPosition.x += 1;
			spawnPosition.z += 1;
		}
		else if (choice == 2) // East
		{
			spawnPosition.x += 1;
			spawnPosition.z += 0;
		}
		else if (choice == 3) // SouthEast
		{
			spawnPosition.x += 1;
			spawnPosition.z -= 1;
		}
		else if (choice == 4) // South
		{
			spawnPosition.x += 0;
			spawnPosition.z -= 1;
		}
		else if (choice == 5) // SouthWest
		{
			spawnPosition.x -= 1;
			spawnPosition.z -= 1;
		}
		else if (choice == 6) // West 
		{
			spawnPosition.x -= 1;
			spawnPosition.z -= 0;
		}
		else if (choice == 7) // NorthWest 
		{
			spawnPosition.x -= 1;
			spawnPosition.z += 1;
		}

		GameObject _cCell = this.generalFactory.InstantiateGameObject(this.generalFactory.Cell,
			spawnPosition, Quaternion.identity);
			
		Cell cCell = _cCell.GetComponent<Cell>();
		cCell.SetBody(this.generalFactory.cellBodyFactory.CellBodySplit(parent.GetBody()));

		// Set new Food
		cCell.FoodReserves = parent.FoodReserves / 2;
		parent.FoodReserves /= 2;
		cCell.GetBody().SetParent(cCell);
								
		cCell.SetEyesCellParent();
		cCell.generateEyes();

		cCell.GetBody().MutationCount++;

		cCell.GetBody().Family = parent.GetBody().Family;

		if (cCell.GetBody().MutationCount >= 10) // Changes to a new Sub Family if mutation counter is bigger than 10 
		{
			Debug.Log("New Sub Family due to heavy mutation");
			cCell.GetBody().SubFamily = this.SubFamilyCounter++;
		}
		else 
			cCell.GetBody().SubFamily = parent.GetBody().SubFamily;

		return cCell;
	}

	internal void CellSpawn()
	{
		int i = 0;
		RandomGenerator rng = new RandomGenerator();

		while (cells.Count < this.generalFactory.CellCount && i < this.generalFactory.SpawnsPerFrame)
		{
			if (rng.GetRandomFloat(1) < 0.5) // In 50% of cases spawn a random cell
				this.spawnRandomCell(); // At a percentage, spawn Highscore cells
			else // Other 50% will spawn a Highscore Cell
			{
				Body newBody = this.CellsHighscore.GetHighscoreBody();
				if (newBody == null) // Highscore not yet initialized, nothing in highscore
					this.spawnRandomCell(); 
				else
					this.spawnCell(newBody);
			}
				

			i++;
		}
	}

	private void spawnCell(Body body)
	{
		Body actualBody = this.DeepClone<Body>(body);

		float xPos = UnityEngine.Random.Range((-1) * this.generalFactory.PlaneSize.x, this.generalFactory.PlaneSize.x);
		float zPos = UnityEngine.Random.Range((-1) * this.generalFactory.PlaneSize.y, this.generalFactory.PlaneSize.y);

		GameObject _cCell = this.generalFactory.InstantiateGameObject(this.generalFactory.Cell, new Vector3(xPos, 0.30f, zPos), Quaternion.identity);

		Cell cCell = _cCell.GetComponent<Cell>();
		cCell.SetBody(actualBody);

		cCell.FoodReserves = this.generalFactory.CellStartingFood;
		cCell.GetBody().SetParent(cCell);

		cCell.SetEyesCellParent();
		cCell.generateEyes();

		cCell.GetBody().Family = body.Family;
		cCell.GetBody().SubFamily = body.SubFamily;

		this.cells.Add(cCell);
	}

	private void spawnRandomCell()
	{
		float xPos = UnityEngine.Random.Range((-1) * this.generalFactory.PlaneSize.x, this.generalFactory.PlaneSize.x);
		float zPos = UnityEngine.Random.Range((-1) * this.generalFactory.PlaneSize.y, this.generalFactory.PlaneSize.y);

		GameObject _cCell = this.generalFactory.InstantiateGameObject(this.generalFactory.Cell, new Vector3(xPos, 0.30f, zPos), Quaternion.identity);
		
		Cell cCell = _cCell.GetComponent<Cell>();
		cCell.FoodReserves = this.generalFactory.CellStartingFood;

		cCell.GetBody().Family = this.FamilyCounter++;
		cCell.GetBody().SubFamily = this.SubFamilyCounter++;

		this.cells.Add(cCell);
	}

	public void AddToDestroy(Cell toDestroy)
	{
		this.cellsToDestroy.Add(toDestroy);
	}

	private void DestroyCell(Cell cellToDestroy)
	{
		// Only add to Highscore if it is a moving cell
		if (cellToDestroy.SpeedSum > this.generalFactory.MinimalMovement)
			this.CellsHighscore.AddToList(cellToDestroy.GetBody());

		this.cells.Remove(cellToDestroy);
		this.generalFactory.DestoryGameObject(cellToDestroy.gameObject);
	}

	public List<Cell> GetCells()
	{
		return this.cells;
	}

	private T DeepClone<T>(T obj)
	{
		using (var ms = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, obj);
			ms.Position = 0;

			return (T)formatter.Deserialize(ms);
		}
	}
}
