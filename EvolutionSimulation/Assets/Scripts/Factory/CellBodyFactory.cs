﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class CellBodyFactory
{
	#region private properties
	private GeneralFactory generalFactory;

	int NeuronStd { get; set; }

	int VisualSensorMu { get; set; }
	int VisualSensorStd { get; set; }
	int VisualRangeMu { get; set; }
	int VisualRangeStd { get; set; }

	int MemoryNeuronMu { get; set; }
	int MemoryNeuronStd { get; set; }

	int MovementActorMu { get; set; }
	int MovementActorStd { get; set; }
	int MovementSpeedMu { get; set; }
	int MovementSpeedStd { get; set; }

	int EatingActorMu { get; set; }
	int EatingActorStd { get; set; }

	float MutationChanceSmall { get; set; }
	float MutationChanceBig { get; set; }
	float MutationAmountSensor { get; set; }
	float MutationAmountNeuron { get; set; }
	float MutationAmountActor { get; set; }
	float OtherLearningRate { get; set; }

	RandomGenerator rng = new RandomGenerator();
	#endregion

	#region public methods
	public CellBodyFactory(GeneralFactory _generalFactory, 
		int _neuronStd, int _visualSensorMu, int _visualSensorStd, int _memoryNeuronMu,
		int _memoryNeuronStd, int _movementActorMu, int _movementActorStd, int _eatingActorMu, int _eatingActorStd,
		int _visualRangeMu, int _visualRangeStd, int _movementSpeedMu, int _movementSpeedStd,

		float _mutationChanceSmall, float _mutationChanceBig, float _mutationAmountSensor,
		float _mutationAmountNeuron, float _mutationAmountActor, float _otherLearningRate)
	{
		this.generalFactory = _generalFactory;

		this.NeuronStd = _neuronStd;
		this.VisualSensorMu = _visualSensorMu;
		this.VisualSensorStd = _visualSensorStd;
		this.MemoryNeuronMu = _memoryNeuronMu;
		this.MemoryNeuronStd = _memoryNeuronStd;
		this.MovementActorMu = _movementActorMu;
		this.MovementActorStd = _movementActorStd;
		this.EatingActorMu = _eatingActorMu;
		this.EatingActorStd = _eatingActorStd;
		this.VisualRangeMu = _visualRangeMu;
		this.VisualRangeStd = _visualRangeStd;
		this.MovementSpeedMu = _movementSpeedMu;
		this.MovementSpeedStd = _movementSpeedStd;

		this.MutationChanceSmall = _mutationChanceSmall;
		this.MutationChanceBig = _mutationChanceBig;
		this.MutationAmountSensor = _mutationAmountSensor;
		this.MutationAmountNeuron = _mutationAmountNeuron;
		this.MutationAmountActor = _mutationAmountActor;
		this.OtherLearningRate = _otherLearningRate;
	}

	public List<Body> generateCellBodies(List<Cell> _parents, int amount)
	{
		List<Body> retList = new List<Body>();

		for (int i = 0; i < amount; i++)
		{
			Body cCell = new Body(_parents[i]);

			List<Sensor> Sensors = this.getRandomSensorSet(_parents[i]);
			List<Actor> Actors = this.getRandomActorsSet();
			List<Neuron> Neurons = this.getRandomNeuronSet((int)(Sensors.Count + Actors.Count) / 2);

			this.ActorsConnectToNeurons(Actors, Neurons.Count);
			this.NeronsConnectToSensors(Neurons, Sensors.Count);

			cCell.Inputs = Sensors;
			cCell.Actors = Actors;
			cCell.Neurons = Neurons;

			retList.Add(cCell);
		}

		return retList;
	}

	public Body GetBody(Cell _parent)
	{
		Body cCell = new Body(_parent);

		List<Sensor> Sensors = this.getRandomSensorSet(_parent);
		List<Actor> Actors = this.getRandomActorsSet();
		List<Neuron> Neurons = this.getRandomNeuronSet((int)(Sensors.Count + Actors.Count) / 2);

		this.ActorsConnectToNeurons(Actors, Neurons.Count);
		this.NeronsConnectToSensors(Neurons, Sensors.Count);

		cCell.Inputs = Sensors;
		cCell.Actors = Actors;
		cCell.Neurons = Neurons;

		return cCell;
	}

	public Body CellBodySplit(Body splittingCellBody)
	{
		// Deep clone of old body
		Body newBody = this.DeepClone<Body>(splittingCellBody);
		
		List<int> removedSensors = new List<int>();
		List<int> removedNeurons = new List<int>();
		List<int> removedActors = new List<int>();

		// Mutate new Sensors (up to 3 at once)
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			newBody.Inputs.AddRange(this.addRandomSensor(newBody.GetParent(), newBody));
			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				newBody.Inputs.AddRange(this.addRandomSensor(newBody.GetParent(), newBody));
				if (this.rng.GetRandomFloat(1) < 0.25)
					newBody.Inputs.AddRange(this.addRandomSensor(newBody.GetParent(), newBody));
			}
		}

		// Mutate to remove old Sensors (up to 5 at once)
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			int removed = this.removeRngSensor(newBody);
			if (removed >= 0)
				removedSensors.Add(removed);

			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				removed = this.removeRngSensor(newBody);
				if (removed >= 0)
					removedSensors.Add(removed);
				if (this.rng.GetRandomFloat(1) < 0.25)
				{
					removed = this.removeRngSensor(newBody);
					if (removed >= 0)
						removedSensors.Add(removed);
					if (this.rng.GetRandomFloat(1) < 0.25)
					{
						removed = this.removeRngSensor(newBody);
						if (removed >= 0)
							removedSensors.Add(removed);
						if (this.rng.GetRandomFloat(1) < 0.125)
						{
							removed = this.removeRngSensor(newBody);
							if (removed >= 0)
								removedSensors.Add(removed);
						}
					}
				}
			}
		}

		// Mutate new Neurons (up to 3 at once)
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			newBody.Neurons.AddRange(this.addRandomNeuron(newBody));
			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				newBody.Neurons.AddRange(this.addRandomNeuron(newBody));
				if (this.rng.GetRandomFloat(1) < 0.25)
					newBody.Neurons.AddRange(this.addRandomNeuron(newBody));
			}
		}
			
		// Mutate to remove old Neurons (up to 5 at once)
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			int removed = this.removeRngNeuron(newBody);
			if (removed >= 0)
				removedActors.Add(removed);
			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				removed = this.removeRngNeuron(newBody);
				if (removed >= 0)
					removedActors.Add(removed);
				if (this.rng.GetRandomFloat(1) < 0.25)
				{
					removed = this.removeRngNeuron(newBody);
					if (removed >= 0)
						removedActors.Add(removed);
					if (this.rng.GetRandomFloat(1) < 0.25)
					{
						removed = this.removeRngNeuron(newBody);
						if (removed >= 0)
							removedActors.Add(removed);
						if (this.rng.GetRandomFloat(1) < 0.125)
						{
							removed = this.removeRngNeuron(newBody);
							if (removed >= 0)
								removedActors.Add(removed);
						}
					}
				}
			}
		}
			

		// Mutate new Actors (up to 3 at once)
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			newBody.Actors.AddRange(this.addRandomActor(newBody));
			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				newBody.Actors.AddRange(this.addRandomActor(newBody));
				if (this.rng.GetRandomFloat(1) < 0.25)
					newBody.Actors.AddRange(this.addRandomActor(newBody));
			}
		}
		
		// Mutate to remove old Actors (up to 5 at once)
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			int removed = this.removeRngActor(newBody);
			if (removed >= 0)
				removedActors.Add(removed);
			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				removed = this.removeRngActor(newBody);
				if (removed >= 0)
					removedActors.Add(removed);
				if (this.rng.GetRandomFloat(1) < 0.25)
				{
					removed = this.removeRngActor(newBody);
					if (removed >= 0)
						removedActors.Add(removed);
					if (this.rng.GetRandomFloat(1) < 0.25)
					{
						removed = this.removeRngActor(newBody);
						if (removed >= 0)
							removedActors.Add(removed);

						if (this.rng.GetRandomFloat(1) < 0.125)
						{
							removed = this.removeRngActor(newBody);
							if (removed >= 0)
								removedActors.Add(removed);
						}
					}
				}
			}
		}

		// Add/Remove new/old weigths, updating just its weigths
		this.ActorsConnectToNeurons(newBody.Actors, newBody.Neurons.Count);
		this.NeronsConnectToSensors(newBody.Neurons, newBody.Inputs.Count);
		this.fixWeightsAfterMutation(newBody, splittingCellBody, removedSensors, removedNeurons, removedActors);


		// Updating existing Weigths, Biases and parameters
		this.UpdateExistingSensors(newBody);
		this.UpdateExistingNeurons(newBody);
		this.UpdateExistingActors(newBody);

		return newBody;
	}

	private void fixWeightsAfterMutation(Body newBody, Body parent, List<int> removedSensors, List<int> removedNeurons, List<int> removedActors)
	{
		// Check if neuron weights fit the inputs. First should be representative
		if (newBody.Inputs.Count != parent.Inputs.Count)
			this.NeronsConnectToSensors(newBody.Neurons, parent.Inputs.Count, parent.Neurons, removedSensors);

		// Check if actor weights fit the neurons. First should be representative
		if (newBody.Neurons.Count != parent.Neurons.Count)
			this.ActorsConnectToNeurons(newBody.Actors, parent.Neurons.Count, parent.Actors, removedNeurons);

		// throw new NotImplementedException();
	}

	private void UpdateExistingActors(Body newBody)
	{
		foreach (Actor cActor in newBody.Actors)
		{
			// For every Actor check if the random roll is bigger than the small mutation chance
			if (this.rng.GetRandomFloat(1) > this.MutationChanceSmall)
				continue;

			// Update Weights
			for (int i = 0; i < cActor.weights.Count; i++)
			{
				if (this.rng.GetRandomFloat(1) < 0.5)
					cActor.weights[i] += rng.GetNormalDistNumber(0, this.OtherLearningRate / 2);
			}

			// Update Bias
			if (this.rng.GetRandomFloat(1) < 0.5)
				cActor.Bias += rng.GetNormalDistNumber(0, this.MutationAmountNeuron / 2);

			// If not bigger, alter value
			if (cActor is MovementActor)
			{
				MovementActor cMovementActor = cActor as MovementActor;
				if (this.rng.GetRandomFloat(1) < 0.5)
					cMovementActor.AddToAngle(rng.GetNormalDistNumber(0, this.MutationAmountActor * (45 / 2)));

				if (this.rng.GetRandomFloat(1) < 0.5)
					cMovementActor.AddToSpeed(rng.GetNormalDistNumber(0, this.MutationAmountActor / 2));
			}
		}
	}

	private void UpdateExistingNeurons(Body newBody)
	{
		foreach (Neuron cNeuron in newBody.Neurons)
		{
			// For every Neuron check if the random roll is bigger than the small mutation chance
			if (this.rng.GetRandomFloat(1) > this.MutationChanceSmall)
				continue;

			// Update Weights
			for (int i = 0; i < cNeuron.weights.Count; i++)
			{
				if (this.rng.GetRandomFloat(1) < 0.5)
					cNeuron.weights[i] += rng.GetNormalDistNumber(0, this.MutationAmountNeuron / 2);
			}

			// Update Bias
			if (this.rng.GetRandomFloat(1) < 0.5)
				cNeuron.Bias += rng.GetNormalDistNumber(0, this.MutationAmountNeuron / 2);
		}
	}

	private void UpdateExistingSensors(Body newBody)
	{
		foreach (Sensor cSensor in newBody.Inputs)
		{
			// For every Sensor check if the random roll is bigger than the small mutation chance
			if (this.rng.GetRandomFloat(1) > this.MutationChanceSmall)
				continue;

			if (cSensor is MemoryNeuron)
			{
				MemoryNeuron cMemoryNeuron = cSensor as MemoryNeuron;

				// Updatign Weigths
				if (cMemoryNeuron.weights != null)
				{
					for (int i = 0; i < cMemoryNeuron.weights.Count; i++)
					{
						if (this.rng.GetRandomFloat(1) < 0.5)
							cMemoryNeuron.weights[i] += rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2);
					}
				}

				// Updating Bias
				cMemoryNeuron.Bias += rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2);
			}
			else if (cSensor is VisualSensor)
			{
				// Updating Eyes
				// Due to this updating that way, the visual cones have to be rebuilt
				VisualSensor cVisualSensor = cSensor as VisualSensor;
				if (this.rng.GetRandomFloat(1) < 0.5)
					cVisualSensor.AddToAngle(rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2));

				if (this.rng.GetRandomFloat(1) < 0.5)
					cVisualSensor.AddToRange(rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2));
			}
		}
	}

	private int removeRngActor(Body _body)
	{
		if (_body.Actors.Count > 0)
		{
			int toRemove = this.rng.GetRandomInt(_body.Actors.Count);
			_body.Actors.RemoveAt(toRemove);

			return toRemove;
		}
		return -1;
	}

	private int removeRngNeuron(Body _body)
	{
		if (_body.Neurons.Count > 0)
		{
			int toRemove = this.rng.GetRandomInt(_body.Neurons.Count);
			_body.Neurons.RemoveAt(toRemove);

			return toRemove;
		}
		return -1;
	}

	private int removeRngSensor(Body _body)
	{
		if (_body.Inputs.Count > 0)
		{
			int toRemove = this.rng.GetRandomInt(_body.Inputs.Count);
			_body.Inputs.RemoveAt(toRemove);

			return toRemove;
		}
		return -1;
	}

	#endregion

	#region private methods

	private T DeepClone<T>(T obj)
	{
		using (var ms = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, obj);
			ms.Position = 0;

			return (T)formatter.Deserialize(ms);
		}
	}

	private IEnumerable<Actor> addRandomActor(Body newBody)
	{
		List<Actor> newActors = new List<Actor>();

		if (this.rng.GetRandomFloat(1) < 0.25)
			newActors.Add(new EatingActor(rng.GetRandomWeight()));
		else
			newActors.Add(new MovementActor(rng, this.MovementSpeedMu, this.MovementSpeedStd, rng.GetRandomWeight()));

		return newActors;
	}

	private IEnumerable<Neuron> addRandomNeuron(Body newBody)
	{
		List<Neuron> newNeurons = new List<Neuron>();

		newNeurons.Add(new Neuron(rng.GetRandomWeight()));

		return newNeurons;
	}

	private IEnumerable<Sensor> addRandomSensor(Cell _parent, Body newBody)
	{
		List<Sensor> newSensors = new List<Sensor>();

		if (this.rng.GetRandomFloat(1) < 0.25)
			newSensors.Add(new MemoryNeuron(rng.GetRandomWeight()));
		else
			newSensors.AddRange(this.generateVisualSensor(_parent));

		return newSensors;
	}

	private List<Sensor> getRandomSensorSet(Cell _parent)
	{
		List<Sensor> retList = new List<Sensor>();

		// Generate random non negative amount of visual sensors
		int rngAmountVisualSensor = -1;
		while (rngAmountVisualSensor < 0)
			rngAmountVisualSensor = (int)rng.GetNormalDistNumber(this.VisualSensorMu, this.VisualSensorStd);

		// Fill the sensor List with visual sensors
		for (int i = 0; i < rngAmountVisualSensor; i++)
			retList.AddRange(this.generateVisualSensor(_parent));

		// Generate random non negative amount of memory neurons
		int rngAmountMemoryNeuron = -1;
		while (rngAmountMemoryNeuron < 0)
			rngAmountMemoryNeuron = (int)rng.GetNormalDistNumber(this.MemoryNeuronMu, this.MemoryNeuronStd);

		// Fill the sensor List with memory neurons
		for (int i = 0; i < rngAmountMemoryNeuron; i++)
			retList.Add(new MemoryNeuron(rng.GetRandomWeight()));

		return retList;
	}

	private List<Sensor> generateVisualSensor(Cell _parent)
	{
		List<Sensor> retList = new List<Sensor>();

		// Every Visual sensor generates two neurons
		for (int j = 0; j < 2; j++)
		{
			if (j == 0)
				retList.Add(new VisualSensor(_parent, rng, this.VisualRangeMu, this.VisualRangeStd, true, rng.GetRandomWeight()));
			else
				retList.Add(new VisualSensor(_parent, rng, this.VisualRangeMu, this.VisualRangeStd, false, rng.GetRandomWeight()));
		}

		return retList;
	}

	private List<Actor> getRandomActorsSet()
	{
		List<Actor> retList = new List<Actor>();

		int rngAmountMovementActor = -1;
		while (rngAmountMovementActor < 0)
			rngAmountMovementActor = (int)rng.GetNormalDistNumber(this.MovementActorMu, this.MovementActorStd);

		for (int i = 0; i < rngAmountMovementActor; i++)
			retList.Add(new MovementActor(rng, this.MovementSpeedMu, this.MovementSpeedStd, rng.GetRandomWeight()));

		int rngAmountEatingActor = -1;
		while (rngAmountEatingActor < 0)
			rngAmountEatingActor = (int)rng.GetNormalDistNumber(this.EatingActorMu, this.EatingActorStd);

		for (int i = 0; i < rngAmountEatingActor; i++)
			retList.Add(new EatingActor(rng.GetRandomWeight()));

		return retList;
	}

	private List<Neuron> getRandomNeuronSet(int amount)
	{
		List<Neuron> retList = new List<Neuron>();

		// Get a random number that is non negative
		int rngAmount = -1;
		while (rngAmount < 0)
			rngAmount = (int)rng.GetNormalDistNumber(amount, this.NeuronStd);

		// Fill the neuron List with neurons
		for (int i = 0; i < rngAmount; i++)
			retList.Add(new Neuron(rng.GetRandomWeight()));

		return retList;
	}

	private void NeronsConnectToSensors(List<Neuron> neurons, int count)
	{
		foreach (Neuron cNeuron in neurons)
		{
			cNeuron.weights = new List<float>();
			cNeuron.X = new List<float>();

			// init every single weigth
			for (int i = 0; i < count; i++)
			{
				cNeuron.weights.Add(rng.GetRandomWeight());
				cNeuron.X.Add(0.0f);
			}
		}
	}

	private void NeronsConnectToSensors(List<Neuron> neurons, int count, List<Neuron> parentNeurons, List<int> removedSensors)
	{
		List<Neuron> cloneParents = this.DeepClone<List<Neuron>>(parentNeurons);

		try // Maybe excatly in the moment of uodate, it got deleted? Happens rather rarely
		{
			// remove already used weight indexes
			foreach (Neuron cNeuron in cloneParents)
				foreach (int cSensorToRemove in removedSensors)
					cNeuron.weights.RemoveAt(cSensorToRemove);
		}
		catch (Exception e)
		{
			Debug.Log("Removing old Sensor Weights in Neurons to Sensor connect failed with: " + e.Message);
		}

		try
		{
			for (int i = 0; i < neurons.Count; i++)
				// init every single weigth
				for (int j = 0; j < count; j++)
					if (cloneParents[i].weights.Count < count) // As long as we can get valeus from the parents get it (Addin gadds to back)
						neurons[i].weights[j] = cloneParents[i].weights[j];
		}
		catch (Exception e)
		{
			Debug.Log("Setting parent inheritance in Neurons to Sensors failed with: " + e.Message);
		}
	}

	private void ActorsConnectToNeurons(List<Actor> actors, int count)
	{
		foreach (Actor cActor in actors)
		{
			cActor.weights = new List<float>();
			cActor.X = new List<float>();

			for (int j = 0; j < count; j++)
			{
				cActor.weights.Add(rng.GetRandomWeight());
				cActor.X.Add(0.0f);
			}
		}
	}

	private void ActorsConnectToNeurons(List<Actor> actors, int count, List<Actor> parentActors, List<int> removedNeurons)
	{
		List<Actor> cloneParents = this.DeepClone<List<Actor>>(parentActors);

		try // Maybe excatly in the moment of uodate, it got deleted? Happens rather rarely
		{
			// remove already used weight indexes
			foreach (Actor cActor in cloneParents)
				foreach (int cNeuronToRemove in removedNeurons)
					cActor.weights.RemoveAt(cNeuronToRemove);
		}
		catch (Exception e)
		{
			Debug.Log("Removing old Neuron Weights in Actors to Neurons connect failed with: " + e.Message);
		}

		try
		{
			for (int i = 0; i < actors.Count; i++)
				for (int j = 0; j < count; j++)
					if (cloneParents[i].weights.Count < count)
						actors[i].weights[j] = cloneParents[i].weights[j];
		}
		catch (Exception e)
		{
			Debug.Log("Setting parent inheritance in Actors to Neurons failed with: " + e.Message);
		}
	}
	#endregion
}
