﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator
{
	private GeneralFactory generalFactory;

	public WorldGenerator(GeneralFactory generalFactory)
	{
		this.generalFactory = generalFactory;
	}

	public void InitWorld()
	{
		this.constructPetriDish();
		this.constructWall();
	}

	private void constructPetriDish()
	{
		GameObject _petriDish = this.generalFactory.InstantiateGameObject(this.generalFactory.PetriDish, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_petriDish.transform.localScale = new Vector3(this.generalFactory.PlaneSize.x * 2, 0.1f, this.generalFactory.PlaneSize.y * 2);

		Debug.Log("Constructed Petri Dish");
	}

	private void constructWall()
	{
		GameObject _wall;

		_wall = this.generalFactory.InstantiateGameObject(this.generalFactory.PetriDishEdge, new Vector3(0, 0, 0), Quaternion.identity);
		_wall.transform.localScale = new Vector3(0.4f, 1f, this.generalFactory.PlaneSize.y * 2);
		_wall.transform.position = new Vector3(this.generalFactory.PlaneSize.x + 0.2f, 0.45f, 0);

		_wall = this.generalFactory.InstantiateGameObject(this.generalFactory.PetriDishEdge, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_wall.transform.localScale = new Vector3(0.4f, 1f, this.generalFactory.PlaneSize.y * 2);
		_wall.transform.position = new Vector3((-1) * this.generalFactory.PlaneSize.x - 0.2f, 0.45f, 0);

		_wall = this.generalFactory.InstantiateGameObject(this.generalFactory.PetriDishEdge, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_wall.transform.localScale = new Vector3(this.generalFactory.PlaneSize.x * 2, 1f, 0.4f);
		_wall.transform.position = new Vector3(0, 0.45f, this.generalFactory.PlaneSize.y + 0.2f);

		_wall = this.generalFactory.InstantiateGameObject(this.generalFactory.PetriDishEdge, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_wall.transform.localScale = new Vector3(this.generalFactory.PlaneSize.x * 2, 1f, 0.4f);
		_wall.transform.position = new Vector3(0, 0.45f, (-1) * this.generalFactory.PlaneSize.y + 0.2f);

		Debug.Log("Constructed Wall");
	}
}
