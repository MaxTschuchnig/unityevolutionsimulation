﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralFactory : MonoBehaviour
{
	public int SpawnsPerFrame;

	// For Environment
	public GameObject PetriDish;
	public GameObject PetriDishEdge;
	public Vector2 PlaneSize;

	WorldGenerator worldGenerator;

	// For Food
	public int FoodCount;
	public GameObject Food;
	public int FoodNoms;

	public FoodFactory foodFactory;

	// For Cells
	public int CellCount;
	public GameObject Cell;
	public float CellStartingFood;
	public int CellReactionTime;

	public CellFactory cellFactory;
	public GameObject VisibleCone;

	// For Bodys
	public CellBodyFactory cellBodyFactory;

	public int NeuronStd;

	public int VisualSensorMu;
	public int VisualSensorStd;
	public int VisualRangeMu;
	public int VisualRangeStd;

	public int MemoryNeuronMu;
	public int MemoryNeuronStd;

	public int MovementActorMu;
	public int MovementActorStd;
	public int MovementSpeedMu;
	public int MovementSpeedStd;

	public int EatingActorMu;
	public int EatingActorStd;

	public float MutationChanceSmall;
	public float MutationChanceBig;
	public float MutationAmountSensor;
	public float MutationAmountNeuron;
	public float MutationAmountActor;
	public float OtherLearningRate;

	// For Mutation
	public float FoodReservesToMutate;
	public int MutationCounterToMutate;

	// For Loss
	public int HighscoreCellAmount;
	public float ComplexityModifier;
	public float VisualModifier;
	public float MovementModifier;
	public float FoodEatenModifier;

	// For minimum Movement
	public float MinimalMovement;

	public int TicksToStore;
	private int count = 0;

	public bool LoadOldData;

	private void Start()
	{
		this.worldGenerator = new WorldGenerator(this);
		this.worldGenerator.InitWorld();

		this.foodFactory = new FoodFactory(this);

		this.cellFactory = new CellFactory(this);

		this.cellBodyFactory = new CellBodyFactory(this, NeuronStd, VisualSensorMu, VisualSensorStd, MemoryNeuronMu, MemoryNeuronStd, MovementActorMu, MovementActorStd,
			EatingActorMu, EatingActorStd, VisualRangeMu, VisualRangeStd, MovementSpeedMu, MovementSpeedStd,
			MutationChanceSmall, MutationChanceBig, MutationAmountSensor, MutationAmountNeuron, MutationAmountActor, OtherLearningRate);

		if (this.LoadOldData)
			this.cellFactory.CellsHighscore.LoadFromFile();
	}

	private void Update()
	{
		this.foodFactory.FoodSpawn();
		this.cellFactory.CellSpawn();
		
		this.foodFactory.UpdateFoods();
		this.cellFactory.UpdateCells();

		// Every 1000 iterations, store data
		if (count > this.TicksToStore)
		{
			Debug.Log("Storing Highscore");
			this.cellFactory.CellsHighscore.StoreToFile();
			count = 0;
		}
		this.count++;
	}

	public GameObject InstantiateGameObject(GameObject toGenerate, Vector3 position, Quaternion direction)
	{
		return Instantiate(toGenerate, position, direction) as GameObject;
	}

	public void DestoryGameObject(GameObject toDestroy)
	{
		Destroy(toDestroy);
	}
}
