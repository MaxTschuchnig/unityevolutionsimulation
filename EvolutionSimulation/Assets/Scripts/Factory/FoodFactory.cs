﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodFactory
{
	private GeneralFactory generalFactory;
	private List<Food> foods = new List<Food>();
	private int generalFoodId = 0;

	public FoodFactory(GeneralFactory generalFactory)
	{
		this.generalFactory = generalFactory;
	}

	internal void UpdateFoods()
	{
		foreach (Food cFood in this.foods)
			cFood.UpdateMe();
	}

	internal void FoodSpawn()
	{
		int cFoodAmount = foods.Count;
		int i = 0;
		while (cFoodAmount < this.generalFactory.FoodCount && i < this.generalFactory.SpawnsPerFrame)
		{
			this.spawnRandomFood();
			i++;
		}
	}

	private void spawnRandomFood()
	{
		float xPos = UnityEngine.Random.Range((-1) * this.generalFactory.PlaneSize.x, this.generalFactory.PlaneSize.x);
		float zPos = UnityEngine.Random.Range((-1) * this.generalFactory.PlaneSize.y, this.generalFactory.PlaneSize.y);

		GameObject _cFood = this.generalFactory.InstantiateGameObject(this.generalFactory.Food, new Vector3(xPos, 0.15f, zPos), Quaternion.identity);
		Food cFood = _cFood.GetComponent<Food>();
		cFood.FoodId = generalFoodId++;
		foods.Add(cFood);
	}

	public void DestroyFood(Food foodToDestroy)
	{
		this.foods.Remove(foodToDestroy);
		this.generalFactory.DestoryGameObject(foodToDestroy.gameObject);
	}
}
