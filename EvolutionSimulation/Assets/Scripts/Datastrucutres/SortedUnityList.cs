﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;

public class SortedUnityList
{
	public List<Body> bodys = new List<Body>();
	int bodysAmount;
	
	public SortedUnityList(int _bodyAmount)
	{
		this.bodysAmount = _bodyAmount;

		Debug.Log("Constructed Highscore List");
		// TODO: Add Families and only allow x percent of highscore to be one family
	}

	public bool AddToList(Body toAdd)
	{
		if (this.bodys.Count < this.bodysAmount || this.bodyIsBigger(toAdd))
		{
			this.Add(toAdd);
			return true;
		}

		return false;
	}

	private void Add(Body toAdd)
	{
		Body storable = this.DeepClone<Body>(toAdd);

		// TODO: Check if there is already 15% of family members.
		// If there is, check if there is already 5% of sub family members
		// If there is, remove the weakest sub family member and add this
		// If there isn't remove weakest family member
		// If nothing applies from here, simply add
		bool familyFilled = this.checkFamiliesFilled(storable);
		bool subFamilyFilled = this.checkSubFamiliesFilled(storable);
		bool noAdd = false;

		if (subFamilyFilled)
		{
			// Check again for score difference (since before we ckecked agains the weakest) and remove weakest of subfamily and replace with current
			int index = this.checkSubFamilyScoreDifferences(storable);
			if (index >= 0)
				this.bodys.RemoveAt(index);
			else
				noAdd = true;
		}
		else if (!subFamilyFilled && familyFilled)
		{
			// Check again for score difference (since before we ckecked agains the weakest) and remove weakest of family and replace with current
			int index = this.checkFamilyScoreDifferences(storable);
			if (index >= 0)
				this.bodys.RemoveAt(index);
			else
				noAdd = true;
		}
		else
		{
			// Regular Remove and add
			if (this.bodys.Count > this.bodysAmount && toAdd.GetScore() > this.bodys[this.bodys.Count - 1].GetScore())
				this.bodys.RemoveAt(this.bodys.Count - 1); // Remove last element
		}

		if (!noAdd)
		{
			this.bodys.Add(storable); // Add as Deep Clone
			this.bodys.Sort(sortByScore);
		}
		
		if (this.bodys.Count > 30)
		{
			string output = "";
			for (int i = 0; i < 10; i++)
				output += this.bodys[i].GetScore().ToString() + ", ";
			output += "..., ";
			for (int i = 0; i < 10; i++)
				output += this.bodys[this.bodys.Count - 10 + i].GetScore().ToString() + ", ";
			Debug.Log(output);
		}
	}

	private int checkFamilyScoreDifferences(Body storable)
	{
		float _score = 0;
		int index = -1;

		List<int> smallerIndices = new List<int>();

		for (int i = 0; i < this.bodys.Count; i++)
		{
			if (storable.Family.Equals(this.bodys[i].Family))
				if (storable.GetScore() > this.bodys[i].GetScore())
					smallerIndices.Add(i);
		}

		if (smallerIndices.Count > 0)
		{
			_score = this.bodys[smallerIndices[0]].GetScore();
			for (int i = 1; i < smallerIndices.Count; i++)
			{
				float cScore = this.bodys[smallerIndices[0]].GetScore();
				if (_score > cScore)
				{
					_score = this.bodys[smallerIndices[i]].GetScore();
					index = i;
				}
			}

			if (index == -1) // All have same score
				index = smallerIndices[smallerIndices.Count - 1]; // Return fartest index
		}

		return index;
	}

	private int checkSubFamilyScoreDifferences(Body storable)
	{
		float _score = 0;
		int index = -1;

		List<int> smallerIndices = new List<int>();

		for (int i = 0; i < this.bodys.Count; i ++)
		{
			if (storable.SubFamily.Equals(this.bodys[i].SubFamily))
				if (storable.GetScore() > this.bodys[i].GetScore())
					smallerIndices.Add(i);
		}

		if (smallerIndices.Count > 0)
		{
			_score = this.bodys[smallerIndices[0]].GetScore();
			for (int i = 1; i < smallerIndices.Count; i ++)
			{
				float cScore = this.bodys[smallerIndices[0]].GetScore();
				if (_score > cScore)
				{
					_score = this.bodys[smallerIndices[i]].GetScore();
					index = i;
				}
			}

			if (index == -1) // All have same score
				index = smallerIndices[smallerIndices.Count - 1]; // Return fartest index
		}
			

		return index;
	}

	private bool checkSubFamiliesFilled(Body storable)
	{
		int count = 0;
		foreach (Body cBody in this.bodys)
			if (cBody.SubFamily.Equals(storable.SubFamily))
				count++;

		if (count < 5)
			return false;
		else
			return true;
	}

	private bool checkFamiliesFilled(Body storable)
	{
		int count = 0;
		foreach (Body cBody in this.bodys)
			if (cBody.Family.Equals(storable.Family))
				count++;

		if (count < 15)
			return false;
		else
			return true;
	}

	private bool bodyIsBigger(Body toAdd)
	{
		foreach (Body cBody in this.bodys)
			if (toAdd.GetScore() > cBody.GetScore())
				return true;

		return false;
	}

	private int sortByScore(Body b1, Body b2)
	{
		return b2.GetScore().CompareTo(b1.GetScore());
	}

	public float GetScores()
	{
		float scoreSum = 0;
		foreach (Body cBody in this.bodys)
			scoreSum += cBody.GetScore();

		return scoreSum;
	}

	public Body GetHighscoreBody()
	{
		if (this.bodys.Count > 0)
		{
			RandomGenerator rng = new RandomGenerator();

			if (rng.GetRandomFloat(1) < 0.5) // 50 % chance to return one from the top 20
			{
				if (this.bodys.Count < 20)
					return this.bodys[rng.GetRandomInt(this.bodys.Count)];
				else
					return this.bodys[rng.GetRandomInt(20)];
			}
			else // other 50% for one of the rest
				return this.bodys[rng.GetRandomInt(this.bodys.Count)];
		}
		return null;
	}

	public void StoreToFile()
	{
		string path = "Resources/CellBodys.bin";
		this.DeleteFile(path);
		using (MemoryStream ms = this.ToByteArray<List<Body>>(this.bodys))
		{
			using (FileStream file = new FileStream(path, FileMode.Create, System.IO.FileAccess.Write))
			{
				byte[] bytes = new byte[ms.Length];
				ms.Read(bytes, 0, (int)ms.Length);
				file.Write(bytes, 0, bytes.Length);
				ms.Close();
			}
		}
	}

	public void LoadFromFile()
	{
		Debug.Log("Loading Highscore");
		try
		{
			string path = "Resources/CellBodys.bin";
			using (MemoryStream ms = new MemoryStream())
			{
				using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
				{
					byte[] bytes = new byte[file.Length];
					file.Read(bytes, 0, (int)file.Length);
					ms.Write(bytes, 0, (int)file.Length);
				}

				this.bodys = this.FromByteArray<List<Body>>(ms);
			}
		}
		catch (Exception e)
		{
			Debug.Log("Error in trying to load from File: " + e.Message);

			this.bodys = new List<Body>();
		}
	}

	void DeleteFile(string filePath)
	{
		// check if file exists
		if (!File.Exists(filePath))
			Debug.Log("File not existing, nothing to delete");
		else
			File.Delete(filePath);
	}

	private T DeepClone<T>(T obj)
	{
		using (var ms = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, obj);
			ms.Position = 0;

			return (T)formatter.Deserialize(ms);
		}
	}

	private MemoryStream ToByteArray<T>(T obj)
	{
		var ms = new MemoryStream();

		var formatter = new BinaryFormatter();
		formatter.Serialize(ms, obj);
		ms.Position = 0;

		return ms;
	}

	private T FromByteArray<T>(MemoryStream obj)
	{
		obj.Position = 0; 

		var formatter = new BinaryFormatter();
		return (T)formatter.Deserialize(obj);
	}
}
