﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Body
{
	#region public properties
	public List<Sensor> Inputs { get; set; }
	public List<Neuron> Neurons { get; set; }
	public List<Actor> Actors { get; set; }
	#endregion

	#region public methods
	public Body()
	{
		this.Inputs = new List<Sensor>();
		this.Neurons = new List<Neuron>();
		this.Actors = new List<Actor>();
	}
	#endregion
}
