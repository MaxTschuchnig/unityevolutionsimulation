﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cells
{
	class Program
	{
		static void Main(string[] args)
		{
			int NeuronStd = 1;

			int VisualSensorMu = 2;
			int VisualSensorStd = 1;
			int VisualRangeMu = 10;
			int VisualRangeStd = 2;

			int MemoryNeuronMu = 0;
			int MemoryNeuronStd = 1;

			int MovementActorMu = 4;
			int MovementActorStd = 2;
			int MovementSpeedMu = 2;
			int MovementSpeedStd = 2;

			int EatingActorMu = 1;
			int EatingActorStd = 1;

			 
			CellBodyFactory cellFact = new CellBodyFactory(NeuronStd, VisualSensorMu, VisualSensorStd, MemoryNeuronMu, MemoryNeuronStd, MovementActorMu, MovementActorStd, 
				EatingActorMu, EatingActorStd, VisualRangeMu, VisualRangeStd, MovementSpeedMu, MovementSpeedStd, 0.1f, 0.01f, 1f, 1f, 1f, 0.1f);
			
			/*
			RandomGenerator rng = new RandomGenerator();
			Console.WriteLine(rng.GetRandomFloat(1));
			Console.WriteLine(rng.GetNormalDistNumber(0, 1));
			*/

			var test = cellFact.generateCellBodies(10);

			/*
			test.Add(cellFact.CellSplit(test[9]));
			test.Add(cellFact.CellSplit(test[10]));
			test.Add(cellFact.CellSplit(test[11]));
			*/
			
			for (int i = 0; i < 10; i ++)
			{
				Console.WriteLine(i);
				test.Add(cellFact.CellSplit(test[i]));
			}

			Console.WriteLine("Done");
		}
	}
}
