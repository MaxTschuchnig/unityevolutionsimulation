﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class CellBodyFactory
{
	#region private properties
	int NeuronStd				{ get; set; }

	int VisualSensorMu			{ get; set; }
	int VisualSensorStd			{ get; set; }
	int VisualRangeMu			{ get; set; }
	int VisualRangeStd			{ get; set; }

	int MemoryNeuronMu			{ get; set; }
	int MemoryNeuronStd			{ get; set; }

	int MovementActorMu			{ get; set; }
	int MovementActorStd		{ get; set; }
	int MovementSpeedMu			{ get; set; }
	int MovementSpeedStd		{ get; set; }

	int EatingActorMu			{ get; set; }
	int EatingActorStd			{ get; set; }

	float MutationChanceSmall	{ get; set; }
	float MutationChanceBig		{ get; set; }
	float MutationAmountSensor	{ get; set; }
	float MutationAmountNeuron	{ get; set; }
	float MutationAmountActor	{ get; set; }
	float OtherLearningRate		{ get; set; }

	RandomGenerator rng = new RandomGenerator();
	#endregion

	#region public methods
	public CellBodyFactory(int _neuronStd, int _visualSensorMu, int _visualSensorStd, int _memoryNeuronMu,
		int _memoryNeuronStd, int _movementActorMu, int _movementActorStd, int _eatingActorMu, int _eatingActorStd,
		int _visualRangeMu, int _visualRangeStd, int _movementSpeedMu, int _movementSpeedStd,

		float _mutationChanceSmall, float _mutationChanceBig, float _mutationAmountSensor,
		float _mutationAmountNeuron, float _mutationAmountActor, float _otherLearningRate)
	{
		this.NeuronStd				= _neuronStd;
		this.VisualSensorMu			= _visualSensorMu;
		this.VisualSensorStd		= _visualSensorStd;
		this.MemoryNeuronMu			= _memoryNeuronMu;
		this.MemoryNeuronStd		= _memoryNeuronStd;
		this.MovementActorMu		= _movementActorMu;
		this.MovementActorStd		= _movementActorStd;
		this.EatingActorMu			= _eatingActorMu;
		this.EatingActorStd			= _eatingActorStd;
		this.VisualRangeMu			= _visualRangeMu;
		this.VisualRangeStd			= _visualRangeStd;
		this.MovementSpeedMu		= _movementSpeedMu;
		this.MovementSpeedStd		= _movementSpeedStd;

		this.MutationChanceSmall	= _mutationChanceSmall;
		this.MutationChanceBig		= _mutationChanceBig;
		this.MutationAmountSensor	= _mutationAmountSensor;
		this.MutationAmountNeuron	= _mutationAmountNeuron;
		this.MutationAmountActor	= _mutationAmountActor;
		this.OtherLearningRate		= _otherLearningRate;
	}

	public List<Body> generateCellBodies(int amount)
	{
		List<Body> retList = new List<Body>();

		for (int i = 0; i < amount; i ++)
		{
			Body cCell = new Body();

			List<Sensor> Sensors = this.getRandomSensorSet();
			List<Actor> Actors = this.getRandomActorsSet();
			List<Neuron> Neurons = this.getRandomNeuronSet((int)(Sensors.Count + Actors.Count) / 2);

			this.ActorsConnectToNeurons(Actors, Neurons.Count);
			this.NeronsConnectToSensors(Neurons, Sensors.Count);

			cCell.Inputs = Sensors;
			cCell.Actors = Actors;
			cCell.Neurons = Neurons;

			retList.Add(cCell);
		}

		return retList;
	}

	public Body CellSplit(Body splittingCell)
	{
		// Rng for changes to existing body
		Body newBody = this.DeepClone<Body>(splittingCell);

		this.UpdateExistingSensors(newBody);
		this.UpdateExistingNeurons(newBody);
		this.UpdateExistingActors(newBody);

		// Rng for new sensors, every cell split there is a chance of generating up to 3 new sensors
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			Console.WriteLine("New Sensor");
			newBody.Inputs.AddRange(this.addRandomSensor(newBody));

			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				newBody.Inputs.AddRange(this.addRandomSensor(newBody));

				if (this.rng.GetRandomFloat(1) < 0.25)
					newBody.Inputs.AddRange(this.addRandomSensor(newBody));
			}
		}

		// Rng for new neurons, every cell split there is a chance of generating up to 3 new neurons
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			Console.WriteLine("New Neuron");
			newBody.Neurons.AddRange(this.addRandomNeuron(newBody));

			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				newBody.Neurons.AddRange(this.addRandomNeuron(newBody));

				if (this.rng.GetRandomFloat(1) < 0.25)
					newBody.Neurons.AddRange(this.addRandomNeuron(newBody));
			}
		}

		// Rng for new actors, every cell split there is a chance of generating up to 3 new actors
		if (this.rng.GetRandomFloat(1) < this.MutationChanceBig)
		{
			Console.WriteLine("New Actor");
			newBody.Actors.AddRange(this.addRandomActor(newBody));

			if (this.rng.GetRandomFloat(1) < 0.5)
			{
				newBody.Actors.AddRange(this.addRandomActor(newBody));

				if (this.rng.GetRandomFloat(1) < 0.25)
					newBody.Actors.AddRange(this.addRandomActor(newBody));
			}
		}

		return newBody;
	}
	
	#endregion

	#region private methods

	private T DeepClone<T>(T obj)
	{
		using (var ms = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, obj);
			ms.Position = 0;

			return (T)formatter.Deserialize(ms);
		}
	}

	private void UpdateExistingActors(Body newBody)
	{
		// Rng for change in existing actors
		foreach (Actor cActor in newBody.Actors)
		{
			// For every Actor check if the random roll is bigger than the small mutation chance
			if (this.rng.GetRandomFloat(1) > this.MutationChanceSmall)
				continue;

			// If not bigger, alter value
			if (cActor is MovementActor)
			{
				Console.WriteLine("Updating Movement Actor");

				MovementActor cMovementActor = cActor as MovementActor;
				if (this.rng.GetRandomFloat(1) < 0.5)
					cMovementActor.AddToAngle(rng.GetNormalDistNumber(0, this.MutationAmountActor * (45 / 2)));

				if (this.rng.GetRandomFloat(1) < 0.5)
					cMovementActor.AddToSpeed(rng.GetNormalDistNumber(0, this.MutationAmountActor / 2));
			}

			// Update Weights
			for (int i = 0; i < cActor.weights.Count; i++)
			{
				if (this.rng.GetRandomFloat(1) < 0.5)
					cActor.weights[i] += rng.GetNormalDistNumber(0, this.OtherLearningRate / 2);
			}

			// Update Bias
			if (this.rng.GetRandomFloat(1) < 0.5)
				cActor.Bias += rng.GetNormalDistNumber(0, this.MutationAmountNeuron / 2);

			/* Not really needed, nothing to update
			else if (cActor is EatingActor)
			{
				Console.WriteLine("Updating Eating Actor");

				EatingActor cEatingActor = cActor as EatingActor;
			}
			*/
		}
	}

	private void UpdateExistingNeurons(Body newBody)
	{
		// Rng for change in existing neurons
		foreach (Neuron cNeuron in newBody.Neurons)
		{
			// For every Neuron check if the random roll is bigger than the small mutation chance
			if (this.rng.GetRandomFloat(1) > this.MutationChanceSmall)
				continue;

			Console.WriteLine("Updating Memory Neuron");

			// If not bigger, alter value
			for (int i = 0; i < cNeuron.weights.Count; i++)
			{
				if (this.rng.GetRandomFloat(1) < 0.5)
					cNeuron.weights[i] += rng.GetNormalDistNumber(0, this.MutationAmountNeuron / 2);
			}

			// Update Bias
			if (this.rng.GetRandomFloat(1) < 0.5)
				cNeuron.Bias += rng.GetNormalDistNumber(0, this.MutationAmountNeuron / 2);
		}
	}

	private void UpdateExistingSensors(Body newBody)
	{
		// Rng for change in existing sensors
		foreach (Sensor cSensor in newBody.Inputs)
		{
			// For every Sensor check if the random roll is bigger than the small mutation chance
			if (this.rng.GetRandomFloat(1) > this.MutationChanceSmall)
				continue;

			// If not bigger, alter value
			if (cSensor is MemoryNeuron)
			{
				Console.WriteLine("Updating Memory Neuron");

				MemoryNeuron cMemoryNeuron = cSensor as MemoryNeuron;
				if (cMemoryNeuron.weights != null)
				{
					for (int i = 0; i < cMemoryNeuron.weights.Count; i++)
					{
						if (this.rng.GetRandomFloat(1) < 0.5)
							cMemoryNeuron.weights[i] += rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2);
					}
				}
			}
			else if (cSensor is VisualSensor)
			{
				Console.WriteLine("Updating Visual Sensor");

				VisualSensor cVisualSensor = cSensor as VisualSensor;
				if (this.rng.GetRandomFloat(1) < 0.5)
					cVisualSensor.AddToAngle(rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2));

				if (this.rng.GetRandomFloat(1) < 0.5)
					cVisualSensor.AddToRange(rng.GetNormalDistNumber(0, this.MutationAmountSensor / 2));
			}
		}
	}

	private IEnumerable<Actor> addRandomActor(Body newBody)
	{
		List<Actor> newActors = new List<Actor>();

		if (this.rng.GetRandomFloat(1) < 0.25)
			newActors.Add(new EatingActor());
		else
			newActors.Add(new MovementActor(rng, this.MovementSpeedMu, this.MovementSpeedStd));

		return newActors;
	}

	private IEnumerable<Neuron> addRandomNeuron(Body newBody)
	{
		List<Neuron> newNeurons = new List<Neuron>();

		newNeurons.Add(new Neuron());

		return newNeurons;
	}

	private IEnumerable<Sensor> addRandomSensor(Body newBody)
	{
		List<Sensor> newSensors = new List<Sensor>();

		if (this.rng.GetRandomFloat(1) < 0.25)
			newSensors.Add(new MemoryNeuron());
		else
			newSensors.AddRange(this.generateVisualSensor());

		return newSensors;
	}

	private List<Sensor> getRandomSensorSet()
	{
		List<Sensor> retList = new List<Sensor>();

		// Generate random non negative amount of visual sensors
		int rngAmountVisualSensor = -1;
		while (rngAmountVisualSensor < 0)
			rngAmountVisualSensor = (int)rng.GetNormalDistNumber(this.VisualSensorMu, this.VisualSensorStd);

		// Fill the sensor List with visual sensors
		for (int i = 0; i < rngAmountVisualSensor; i++)
			retList.AddRange(this.generateVisualSensor());

		// Generate random non negative amount of memory neurons
		int rngAmountMemoryNeuron = -1;
		while (rngAmountMemoryNeuron < 0)
			rngAmountMemoryNeuron = (int)rng.GetNormalDistNumber(this.MemoryNeuronMu, this.MemoryNeuronStd);

		// Fill the sensor List with memory neurons
		for (int i = 0; i < rngAmountMemoryNeuron; i++)
			retList.Add(new MemoryNeuron());

		return retList;
	}

	private List<Sensor> generateVisualSensor()
	{
		List<Sensor> retList = new List<Sensor>();

		// Every Visual sensor generates two neurons
		for (int j = 0; j < 2; j++)
		{
			if (j == 0)
				retList.Add(new VisualSensor(rng, this.VisualRangeMu, this.VisualRangeStd, true));
			else
				retList.Add(new VisualSensor(rng, this.VisualRangeMu, this.VisualRangeStd, false));
		}

		return retList;
	}

	private List<Actor> getRandomActorsSet()
	{
		List<Actor> retList = new List<Actor>();

		int rngAmountMovementActor = -1;
		while (rngAmountMovementActor < 0)
			rngAmountMovementActor = (int)rng.GetNormalDistNumber(this.MovementActorMu, this.MovementActorStd);

		for (int i = 0; i < rngAmountMovementActor; i++)
			retList.Add(new MovementActor(rng, this.MovementSpeedMu, this.MovementSpeedStd));

		int rngAmountEatingActor = -1;
		while (rngAmountEatingActor < 0)
			rngAmountEatingActor = (int)rng.GetNormalDistNumber(this.EatingActorMu, this.EatingActorStd);

		for (int i = 0; i < rngAmountEatingActor; i++)
			retList.Add(new EatingActor());

		return retList;
	}

	private List<Neuron> getRandomNeuronSet(int amount)
	{
		List<Neuron> retList = new List<Neuron>();

		// Get a random number that is non negative
		int rngAmount = -1;
		while (rngAmount < 0)
			rngAmount = (int)rng.GetNormalDistNumber(amount, this.NeuronStd);
		
		// Fill the neuron List with neurons
		for (int i = 0; i < rngAmount; i ++)
			retList.Add(new Neuron());

		return retList;
	}

	private void NeronsConnectToSensors(List<Neuron> neurons, int count)
	{
		foreach (Neuron cNeuron in neurons)
		{
			cNeuron.weights = new List<float>();
			cNeuron.X = new List<float>();

			// init every single weigth
			for (int i = 0; i < count; i ++)
			{
				cNeuron.weights.Add(rng.GetRandomWeight());
				cNeuron.X.Add(rng.GetRandomWeight());
			}
				
		}
	}

	private void ActorsConnectToNeurons(List<Actor> actors, int count)
	{
		foreach (Actor cActor in actors)
		{
			cActor.weights = new List<float>();
			cActor.X = new List<float>();

			for (int i = 0; i < count; i++)
			{
				cActor.weights.Add(rng.GetRandomWeight());
				cActor.X.Add(rng.GetRandomWeight());
			}
			
		}
	}
	#endregion
}
