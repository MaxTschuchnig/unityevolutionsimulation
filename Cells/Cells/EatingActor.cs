﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class EatingActor : Actor
{
	#region public methods
	public override void Act()
	{
		throw new System.NotImplementedException();
	}

	// Eating action takes 2 Energy 
	public override double GetEnergyConsumption()
	{
		return 2;
	}

	public override double GetActivation()
	{
		double activation = 0;

		for (int i = 0; i < this.weights.Count; i++)
		{
			// Implizit casting to double
			activation += this.weights[i] * this.X[i];
		}
		activation += Bias;

		// Return the applied logistic transfer function
		return 1 / 1 + System.Math.Exp((-1) * activation);
	}
	#endregion
}
