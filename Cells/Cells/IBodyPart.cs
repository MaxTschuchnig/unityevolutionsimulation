﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public abstract class IBodyPart
{
	#region public properties
	public List<float> X;
	public float Bias;
	#endregion

	#region public methods
	public abstract double GetEnergyConsumption();
	#endregion
}
