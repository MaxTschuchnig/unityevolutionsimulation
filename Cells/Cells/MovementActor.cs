﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class MovementActor : Actor
{
	#region private properties
	float Direction { get; set; }
	float Speed { get; set; }
	#endregion

	#region public methods
	public MovementActor(RandomGenerator rand, float _movementSpeedMu, float _movementSpeedstd)
	{
		this.Direction = rand.GetRandomFloat(360);
		this.Speed = rand.GetNormalDistNumber(_movementSpeedMu, _movementSpeedstd);
	}

	public override void Act()
	{
		throw new System.NotImplementedException();
	}

	// Consumes 0.5 base + 1.5^Speed
	public override double GetEnergyConsumption()
	{
		double consumption = 0.5 + Math.Pow(1.5, this.Speed);

		return consumption;
	}

	public override double GetActivation()
	{
		double activation = 0;

		for (int i = 0; i < this.weights.Count; i++)
		{
			// Implizit casting to double
			activation += this.weights[i] * this.X[i];
		}
		activation += Bias;

		// Return the applied logistic transfer function
		return 1 / 1 + System.Math.Exp((-1) * activation);
	}

	public void AddToAngle(float angle)
	{
		this.Direction += angle;
	}

	public void AddToSpeed(float _speed)
	{
		this.Speed += _speed;
	}
	#endregion
}
