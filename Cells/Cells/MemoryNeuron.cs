﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class MemoryNeuron : Sensor
{
	#region public properties
	public List<float> weights { get; set; }
	#endregion

	#region public methods
	// Membering takes 1 Energy
	public override double GetEnergyConsumption()
	{
		return 1;
	}
	#endregion
}
