﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class VisualSensor : Sensor
{
	#region private properties
	float Angle			{ get; set; }
	float Range			{ get; set; }
	bool TypeClosest	{ get; set; }
	#endregion

	#region public methods
	public VisualSensor(RandomGenerator rand, float _visualRangeMu, float _visualRangeStd, bool _typeClosest)
	{
		this.Angle = rand.GetRandomFloat(360);
		this.Range = rand.GetNormalDistNumber(_visualRangeMu, _visualRangeStd);
		this.TypeClosest = _typeClosest;
	}

	// Looking takes 0.1 base + 0.1 * Angle if smaller 85 else 2^Range + 1.1^(Range)
	public override double GetEnergyConsumption()
	{
		double consumption = 0.1 + Math.Pow(1.1, this.Range);

		if (this.Angle <= 85)
			consumption += (0.1 * this.Angle);
		else
			consumption += Math.Pow(2, this.Angle - 85 + (0.1 * this.Angle));

		return consumption;
	}

	public void AddToAngle(float _angle)
	{
		this.Angle += _angle;
	}

	public void AddToRange(float _range)
	{
		this.Range += _range;
	}
	#endregion
}