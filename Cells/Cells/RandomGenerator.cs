﻿using System;
using System.Collections;
using System.Collections.Generic;

public class RandomGenerator
{
	#region private properties
	System.Random rand = new System.Random();
	#endregion

	#region public methods
	public float GetNormalDistNumber( double mean, double stdDev)
	{
		double u1 = 1.0 - rand.NextDouble();                                                            //uniform(0,1] random doubles
		double u2 = 1.0 - rand.NextDouble();
		float randStdNormal = (float)(Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2));   //random normal(0,1)
		float randNormal = (float)mean + (float)stdDev * randStdNormal;                                 //random normal(mean,stdDev^2)

		return randNormal;
	}

	public bool GetRandomBool()
	{
		return (rand.NextDouble() > 0.5f);
	}

	public float GetRandomWeight()
	{
		// Is weight activated or not
		double mu = 0;
		if (this.GetRandomBool())
			mu = 1;

		double std = 0.25;
		return GetNormalDistNumber(mu, std);
	}

	public float GetRandomFloat(int upper)
	{
		return (float) (rand.NextDouble() * upper);
	}
	#endregion
}
